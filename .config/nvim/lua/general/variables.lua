-- =============================================================================
--  __      __________   ______
-- /  \    /  \_____  \ /  __  \
-- \   \/\/   //  ____/ >      <
--  \        //       \/   --   \
--   \__/\  / \_______ \______  /
--        \/          \/      \/
-- variables.lua --- Variable declarations for easier interaction with the vim API
-- Copyright (c) 2021 Sourajyoti Basak
-- Author: Sourajyoti Basak < wiz28@protonmail.com >
-- URL: https://github.com/wizard-28/dotfiles/
-- License: MIT
-- =============================================================================

cmd = vim.cmd 	-- alias for executing vim commands
fn = vim.fn	-- alias for calling vim functions
g = vim.g	-- alias to access global variables
opt = vim.opt	-- alias to set options
api = vim.api	-- alias to access the vim API

--wk = require('which-key')
