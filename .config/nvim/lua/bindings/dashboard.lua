wk.register({
	['<leader>s'] = { name = '+Session'},
	['<leader>ss'] = { '<cmd>SessionSave<cr>', 'Save session' },
	['<leader>sl'] = { '<cmd>SessionLoad<cr>', 'Load session' },

	['<leader>cn'] = { '<cmd>DashboardNewFile<cr>', 'Create new file' }
})
