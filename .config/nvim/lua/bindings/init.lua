wk = require('which-key')
wk.setup {}

require('bindings.telescope')
require('bindings.undotree')
require('bindings.gitsigns')
require('bindings.dashboard')
